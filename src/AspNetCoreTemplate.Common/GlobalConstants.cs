﻿namespace AspNetCoreTemplate.Common
{
    public static class GlobalConstants
    {
        public const string SystemName = "CAMPHIA 2023 STAFF PORTAL";

        public const string AdministratorRoleName = "Administrator";
    }
}
